calculate = function()
{

    var GoLive = document.getElementById('GoLive').value;
    var DefWks = parseInt(document.getElementById('SemanasDefinicion').value);
    var DesWks = parseInt(document.getElementById('SemanasDesarrollo').value);
    var MigWks = parseInt(document.getElementById('SemanasMigracion').value);
    var CapWks = parseInt(document.getElementById('SemanasCapacitacion').value);
    var ImpWks = parseInt(document.getElementById('SemanasImplantacion').value);

    var dateGoLive = new Date(GoLive);
    var DateCalcula = new Date(GoLive);

    /*console.log(DefWks);
     console.log(DesWks);
     console.log(MigWks);
     console.log(CapWks);
     console.log(ImpWks);
     */

    var dd = dateGoLive.getDate()+1;
    var mm = dateGoLive.getMonth()+1;
    var y  = dateGoLive.getFullYear();
    var diasrestaDef = 0
    var diasrestaDef2 = 0
    var diasrestaDes = 0
    var diasrestaDes2 = 0
    var diasrestaMig = 0
    var diasrestaMig2 = 0    
    var diasrestaCap = 0
    var diasrestaCap2 = 0
    var diasrestaImp = 0
    var diasrestaImp2 = 0
    
    var GoLiveTexto =  y + '-' + mm + '-' + dd;



    // Calcula Fecha Defincion
    diasrestaDef = -(DefWks+DesWks+MigWks+CapWks+ImpWks)
    var momentDef = moment(GoLive).add(diasrestaDef, 'w').format('YYYY-MM-DD')
    
    var DefFInicial =  momentDef;

    diasrestaDef2 = -(DesWks+MigWks+CapWks+ImpWks)
    var momentDef2 = moment(GoLive).add(diasrestaDef2, 'w').format('YYYY-MM-DD')

    var DefFFinal =  momentDef2;

    // Calcula Fecha Desarrollo
    diasrestaDes =  -(DesWks+MigWks+CapWks+ImpWks)
    var momentDes = moment(GoLive).add(diasrestaDes, 'w').format('YYYY-MM-DD')
    
    var DesFInicial =  momentDes;

    diasrestaDes2 = -(MigWks+CapWks+ImpWks)
    var momentDes2 = moment(GoLive).add(diasrestaDes2, 'w').format('YYYY-MM-DD')
    
    var DesFFinal =  momentDes2;

    // Calcula Fecha Migracion
    diasrestaMig = - (MigWks+CapWks+ImpWks)
    var momentMig = moment(GoLive).add(diasrestaMig, 'w').format('YYYY-MM-DD')

    var MigFInicial =  momentMig;

    diasrestaMig2 = -(CapWks+ImpWks)
    var momentMig2 = moment(GoLive).add(diasrestaMig2, 'w').format('YYYY-MM-DD')

    var MigFFinal =  momentMig2;

    // Calcula Fecha Capacitacion

    diasrestaCap = - (CapWks+ImpWks)
    var momentCap = moment(GoLive).add(diasrestaCap, 'w').format('YYYY-MM-DD')

    var CapFInicial =  momentCap

    diasrestaCap2 =  -(ImpWks)
    var momentCap2 = moment(GoLive).add(diasrestaCap2, 'w').format('YYYY-MM-DD')

    var CapFFinal =  momentCap2;

    // Calcula Fecha Implantacion
    diasrestaImp = -(ImpWks)
    var momentImp = moment(GoLive).add(diasrestaImp, 'w').format('YYYY-MM-DD')

    var ImpFInicial =  momentImp;


    diasrestaImp2 = -(0)
    var momentImp2 = moment(GoLive).add(diasrestaImp2, 'w').format('YYYY-MM-DD')

    var ImpFFinal =  momentImp2;

    //console.log("Hello world!");

    document.getElementById('DefFechaInicial').value = DefFInicial;
    document.getElementById('DefFechaFinal').value = DefFFinal;

    document.getElementById('DesFechaInicial').value = DesFInicial;
    document.getElementById('DesFechaFinal').value = DesFFinal;

    document.getElementById('MigFechaInicial').value = MigFInicial;
    document.getElementById('MigFechaFinal').value = MigFFinal;

    document.getElementById('CapFechaInicial').value = CapFInicial;
    document.getElementById('CapFechaFinal').value = CapFFinal;

    document.getElementById('ImpFechaInicial').value = ImpFInicial;
    document.getElementById('ImpFechaFinal').value = ImpFFinal;

}
